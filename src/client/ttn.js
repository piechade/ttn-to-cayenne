const events = require('events');
const ttn = require("ttn");


module.exports = class TtnClient extends events.EventEmitter {

    async regDevice(appID, accessKey) {
        const client = await ttn.data(appID, accessKey);

        // discover handler and open mqtt connection
        client.on('uplink', (devID, payload) => {
            console.log('Received uplink from ', devID);
            console.log(payload);
            this.emit(appID, payload);
        });

        const application = await ttn.application(appID, accessKey);

        // discover handler and open application manager client
        // get the application info
        const app = await application.get();
        console.log(app);

        // list the apps devices
        const devices = await application.devices();
        console.log(devices);

        return true;
    }
}