const events = require('events');
const Cayenne = require('cayennejs');

module.exports = class CayenneClient extends events.EventEmitter {

    async connect(param) {
        // Initiate MQTT API
        this.cayenneClient = await new Cayenne.MQTT(param);

        const test = await this.cayenneClient.connect((err, mqttClient) => {
            if(err) console.error(err);
        })

        return this.cayenneClient;
    }
}