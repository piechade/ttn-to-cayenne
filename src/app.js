const TtnClient = require('./client/ttn')
const CayenneClient = require('./client/cayenne')

async function main() {
  const appID = 'test_hs_ma';
  const accessKey =
  'ttn-account-v2.Pe6xv5tcgjr5WezVKNzpmjM9jlPjT5SVy1PMo3_QGr4';

  const login = {
    username: "087f3ab0-8e6f-11e8-88f5-91483cc3d606",
    password: "d4651cbd7874a91c0efef8559c15cd388cb2f691",
    clientId: "0d789340-8e6f-11e8-87b0-97a7dd91c15a"
  }
  
  const ttn = new TtnClient();
  const cayenne = await new CayenneClient().connect(login)

  ttn.on(appID, (payload) => {
    if (cayenne.connected === true) {
      cayenne.celsiusWrite(1, payload.payload_fields.p25);
      cayenne.rawWrite(2, payload.payload_fields.p10,"rel_hum", "p");
    }
  });
  await ttn.regDevice(appID, accessKey);
}

main().catch(err => {
  console.error(err);
});